// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Dice.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Dice, "Dice" );
 