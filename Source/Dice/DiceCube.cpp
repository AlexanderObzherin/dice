// Fill out your copyright notice in the Description page of Project Settings.

#include "DiceCube.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMeshSocket.h"
#include "Runtime/Engine/Classes/Components/TextRenderComponent.h"


// Sets default values
ADiceCube::ADiceCube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create static mesh component
	DiceCubeMesh = CreateDefaultSubobject<UStaticMeshComponent>( TEXT( "DiceCubeMesh" ) );
	RootComponent = DiceCubeMesh;

	mTextAbove = CreateDefaultSubobject<UTextRenderComponent>( TEXT( "TextRenderComponent" ) );
	mTextAbove->AttachTo( RootComponent );
	
	GetMesh()->SetSimulatePhysics( true );

}

// Called when the game starts or when spawned
void ADiceCube::BeginPlay()
{
	Super::BeginPlay();

	mTextAbove->SetText( TEXT("New Text") );
	mTextAbove->SetHorizontalAlignment( EHTA_Center );
	mTextAbove->SetWorldSize( 100.0f );
	mTextAbove->SetTextRenderColor( FColor::Red );
	mTextAbove->bAbsoluteRotation = true;
	
	//Initialize sockets in array
	mPlaneSockets.Add( GetMesh()->GetSocketByName( FName( "1" ) ) );
	mPlaneSockets.Add( GetMesh()->GetSocketByName( FName( "2" ) ) );
	mPlaneSockets.Add( GetMesh()->GetSocketByName( FName( "3" ) ) );
	mPlaneSockets.Add( GetMesh()->GetSocketByName( FName( "4" ) ) );
	mPlaneSockets.Add( GetMesh()->GetSocketByName( FName( "5" ) ) );
	mPlaneSockets.Add( GetMesh()->GetSocketByName( FName( "6" ) ) );

}

// Called every frame
void ADiceCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector newTextPosition = this->GetActorLocation() + FVector( 0.0f, 0.0f, 150.0f );
	mTextAbove->SetWorldLocation( newTextPosition  );

	mTextAbove->SetText( FString::FromInt( DetectUpsidePlane() ) );
	
}

int ADiceCube::DetectUpsidePlane()
{
	//to check if all sockets are initialized
	if( !(mPlaneSockets[0] && mPlaneSockets[1] && mPlaneSockets[2] && mPlaneSockets[3] && mPlaneSockets[4] && mPlaneSockets[5]) )
	{
		return 0;
	}

	//
	int maxZ = 1;

	for( int i = 1; i < 7; i++ )
	{
		FString stringNameCurrent = FString::FromInt( i );
		FString stringNameMax = FString::FromInt( maxZ );

		if( GetMesh()->GetSocketLocation( FName( *stringNameCurrent ) ).Z > GetMesh()->GetSocketLocation( FName( *stringNameMax ) ).Z )
		{
			maxZ = i;
		}
	}
	
	UpsidePlane = maxZ;

	return maxZ;
}

