// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DiceCube.generated.h"


UCLASS()
class DICE_API ADiceCube : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADiceCube();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return DiceCubeMesh; }

	UPROPERTY( VisibleAnywhere, Category = "DiceUpside" )
	int UpsidePlane;

protected:
	int DetectUpsidePlane();

private:
	UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "Dice", meta = (AllowPrivateAccess = "true") )
	class UStaticMeshComponent* DiceCubeMesh;

	TArray<const class UStaticMeshSocket *> mPlaneSockets;

	class UTextRenderComponent* mTextAbove;

};
